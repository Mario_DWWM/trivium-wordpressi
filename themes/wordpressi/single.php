<?php
get_header();
?>

<main>
    <?php the_title( '<h1>', '</h1>' ); ?>

    <div class="wrap">
        <?php
        while ( have_posts() ) : the_post();
        ?>
        <img src="<?php the_field('enluminure') ?>" alt="<?php the_field('nom') ?>" class="big-img">
        <div class="infos">
            <h4>Taille</h4>
            <p class="taille">
                <?php the_field('taille_min'); ?> à <?php the_field('taille_max'); 
                if (get_field('taille_max')>2) {
                    echo ' Toises';
                } else {
                    echo ' Toise';
                }
                ?></p>
            <h4>Description</h4>
            <p class="desc"><?php the_field('description'); ?></p>
            <h4>Proies</h4>
            <div class="proies">
            <?php
            $featured_posts = get_field('proies');
            if( $featured_posts ):
                foreach( $featured_posts as $featured_post ):
                $permalink = get_permalink( $featured_post->ID );
                $custom_field = get_field( 'nom', $featured_post->ID );
                ?>
                <a href="<?php echo $permalink ?>"><p><?php echo esc_html( $custom_field ); ?></p></a>
                <?php endforeach; 
            else :
                
                echo "<p class='noHover'>Aucune</p>";
                
            endif; ?>
            </div>
        </div>
        <?php
        endwhile;
        ?>
    </div>
</main>

<?php
get_footer();
?>