<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head(); ?>
    <?php if(is_home()) {
        echo "<meta name='description' content='Le bestiaire de roy. Toutes les information nécessaires.'>";
        echo "<meta name='description' content='bestiaire, forêt, roy, beste'>";
    } else {
        ?>
        <meta name='description' content='<?php echo get_field("description"); ?>'>
        <meta name='keywords' content='<?php echo get_field("keywords"); ?>'>
        <?php
    }?>
    <title><?php echo get_bloginfo('name');
                    if (!is_home()) { echo ' | '.get_the_title();} ?></title>
</head>
<body>
    <a href="<?php echo home_url(); ?>" class="home-link"><header>Le bestiaire du roy</header></a>