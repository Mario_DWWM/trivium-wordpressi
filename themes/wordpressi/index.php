<?php
get_header();
?>

<main>
    <h1>Index</h1>
    <div class="wrap">
    <?php 
    $args = array(
        'orderby' => 'title',
        'order' => 'ASC'
    );
    $the_query = new WP_Query( $args );
    if ( $the_query->have_posts() ) : 
        while ( $the_query->have_posts() ) : $the_query->the_post(); 
    ?>
        <div class="beste">
            <a href="<?php echo get_post_permalink(); ?>">
                <h2><?php the_title(); ?></h2>
                <img src="<?php the_field('enluminure'); ?>" alt="<?php the_field('nom'); ?>">
            </a>
        </div>
    <?php
        endwhile; 
    endif; 
    ?>
    </div>
</main>

<?php
get_footer();
?>